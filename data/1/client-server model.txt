client-server model
Model in which a powerfull computer called server (often centrally housed and maintained by system admin) stores the data. Clients (with simpler machines) can connect to this server to get acces to remote data. Most popular is a Web application. Applicable if in same building and/or when far apart.
